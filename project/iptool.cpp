#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <strings.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <fstream>
#include <string>
#include <string.h>
#include <vector>
#include <algorithm>
#include "core.h"
using namespace std;

#define MAXLEN 256

int main (int argc, char** argv)
{
    // Check parameters
    if (argc < 4)
    {
        cout << "Syntax: " << argv[0] << " <SRC> <DST> <parameter file> [-g]"
             << endl;
        return -1;
    }

    // Parse options
    COLORSPACE cs = CS_HSI;
    VID_MODE mode = PER_FRAME;
    int c;
    while ((c = getopt(argc, argv, "gs")) != -1)
    {
        switch (c)
        { 
            case 'g': // Assume greyscale processing
                cs = CS_GREY;
                break;
            case 's':
                mode = PER_SOI;
                break;
            default:
                abort();
        }
    }

    // Read Param file
    vector<IPParam> params = loadIPParams(argv[optind + 2]);

    // Check if SRC is image or video
    string srcPath(argv[optind]);
    string ext = getext(srcPath);
    string tgtPath(argv[optind + 1]);
    if (ext == "ppm" || ext == "pgm")
    {
        // ----- Process image ----- 
        try
        {
            utility::processImage(srcPath, tgtPath, params);
        }
        catch (IPException& e)
        {
            cerr << "Error in processing image: " << e.what() << endl;
        }
    }
    else if (ext == "m1v" || ext == "mpg")
    {
        utility::processVideo(srcPath, tgtPath, params, cs, mode);
    }
    else
    {
        cerr << "Unknown image or video type -> " << ext << endl;
    }

    return 0;
}

