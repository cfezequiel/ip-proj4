#! /usr/bin/python

import sys
import math
import os
from iptest import *


# Filter: fftcheck
#   Desc 
#       Convert input image to frequency using FFT and back using IFFT
#       Output is image should be the same as original image
#   Params
#       None

# Filter: fft
#   Desc 
#       Convert input image using FFT
#       Output is image in frequency domain
#   Params
#       None

# Filter: fftlowpass
#   Desc
#       Low-pass filter (circular)
#   Params
#       F       Cut-off frequency

# Filter: ffthighpass
#   Desc 
#       High-pass filter (circular)
#   Params
#       F       Cut-off frequency

# Input files
SRC_IMG_GREY = "input/microscopy_grey_stack1_Z27_800x600.pgm"
SRC_IMG_GREY2 = "input/baboon_grey_512x512.pgm"
SRC_IMG_GREY3 = "input/afghan_girl_250x235.pgm"
SRC_IMG_COLOR = "input/microscopy_color_stack1_Z27_800x600.ppm"
#SRC_IMG_COLOR2 = "input/afghan_girl_250x235.ppm"
SRC_IMG_COLOR2 = "input/afghan_girl_580x580.ppm"
SRC_IMG_COLOR3 = "input/afghan_girl_noise_580x580.ppm"
SRC_VID_GREY = "input/microscopy_grey_stack3_800x600_20.m1v"
SRC_VID_GREY2 = "input/cat_dog_grey_noise_320x240_75.m1v"
SRC_VID_COLOR = "input/demo_short_640x480_21.m1v"

class DFTFilterTestRunner(IPTestRunner):

    def __init__(self, outputDir, confDir):
        IPTestRunner.__init__(self, [])

        self.outputDir = outputDir
        self.confDir = confDir
        self.outputPrefixes = []

    def getOutputPrefix(self, inputFile, filterName, setGreyscaleVideo=False):

        options=""
        colorspace = ""
        filetype = ""
        (path, ext) = os.path.splitext(inputFile)
        if ext == ".pgm":
            colorspace = "grey"
            fileType = "img"
        elif ext == ".ppm":
            colorspace = "color"
            fileType = "img"
        elif ext == ".m1v":
            fileType = "vid"
            if setGreyscaleVideo == True:
                options = "-g"
                colorspace = "grey"
            else:
                colorspace = "color"

        outputPrefix = "%s_%s_%s_%s" % (fileType, colorspace, filterName, os.path.basename(path))
        return (outputPrefix, options)

    def addDFTTest(self, inputFile, roi, filterName, filterParams, setGreyscaleVideo=False,
            soi=(1,1)):
        """Add a DFT filter test.
        
        NOTE: assumes only one parameter per parameter file 
        """
        
        (outputPrefix, options) = self.getOutputPrefix(inputFile, filterName, setGreyscaleVideo)
        if filterParams:
            filterParamsStr = "_".join(["%03d" % param for param in filterParams])
            outputName = "%s_%s" % (outputPrefix, filterParamsStr)
        else:
            outputName = "%s" % (outputPrefix)

        subdir = outputPrefix + ".d"
        try:
            self.outputPrefixes.index(outputPrefix)
        except ValueError:
            self.addSetupCmd("mkdir -p %s/%s" % (self.outputDir, subdir))
            self.outputPrefixes.append(outputPrefix)

        self.add(IPTest(inputFile,
                        self.outputDir + '/' + subdir + '/' + outputName, 
                        IPParams(self.confDir + '/' + outputName + ".p", 
                                 [IPParam(IPFilter(filterName, filterParams), roi, SOI(soi[0], soi[1]))]), 
                        options))

    def addDFTComboTest(self, inputFile, roi, filterName, filterParams, setGreyscaleVideo=False,
            soi=(1,1)):

        # Original test
        self.addDFTTest(inputFile, roi, filterName, filterParams, setGreyscaleVideo, soi)

        # Test with DFT amplitude view
        if filterName.find("_amp") > 0:
            return
        (fftPrefix, filterName_) = filterName.split("_")
        filterNameAmp = fftPrefix + "_amp_" + filterName_
        self.addDFTTest(inputFile, roi, filterNameAmp, filterParams, setGreyscaleVideo, soi)


if __name__ == '__main__':

    # ============== Debug Tests ===============
    if 0:
        trDFTDebug = DFTFilterTestRunner("output", "conf")
        roi = ROI(0,0,500,375)
        trDFTDebug.addDFTComboTest("input/mercedz.pgm", roi, "fft_highpass", [50])
        for T in range(100, 255, 5):
            trDFTDebug.addDFTTest("input/mercedz.pgm", roi, "edgedetect", [T])
        trDFTDebug.run()

    # ============== Simple Tests ===============
    trDFTSimple = DFTFilterTestRunner("output", "conf")
    trDFTSimple.addSetupCmd('cd output && ./clean.sh && cd ..') 

    # FFT Conversion check
    roi = ROI(0,0,800,600)
    trDFTSimple.addDFTTest(SRC_IMG_GREY, roi, "fft_check", [])
    trDFTSimple.addDFTTest(SRC_IMG_COLOR, roi, "fft_check", [])

    # FFT amplitude image check
    roi = ROI(52,33,404,424)
    trDFTSimple.addDFTTest(SRC_IMG_GREY2, roi, "fft_amp", [])

    # low-pass
    roi = ROI(122,145,553,357)
    trDFTSimple.addDFTComboTest(SRC_IMG_GREY, roi, "fft_lowpass", [20])

    # high-pass
    roi = ROI(122,145,553,357)
    trDFTSimple.addDFTComboTest(SRC_IMG_GREY, roi, "fft_highpass", [20])

    # band-pass
    roi = ROI(122,145,553,357)
    trDFTSimple.addDFTComboTest(SRC_IMG_GREY, roi, "fft_bandpass", [20, 40])

    # band-stop
    roi = ROI(122,145,553,357)
    trDFTSimple.addDFTComboTest(SRC_IMG_GREY, roi, "fft_bandstop", [20, 40])

    trDFTSimple.save("simple_test.sh")

    # ============== Tests for Report ===============
    trDFT = DFTFilterTestRunner("output", "conf")
    trDFT.addSetupCmd('cd output && ./clean.sh && cd ..') 

    getMaxRadius = lambda roi: int(math.sqrt(roi.sy * roi.sy + roi.sx * roi.sx))

    # Switch tests on/off
    S = 0

    # image, grey, amplitude, micro
    if 0:
        roi = ROI(0,0,800,600)
        trDFT.addDFTTest(SRC_IMG_GREY, roi, "fft_amp", [])

    # image, grey, lowpass, micro
    if S:
        roi = ROI(122,145,556,358)
        trDFT.addDFTComboTest(SRC_IMG_GREY, roi, "fft_lowpass", [0])
        trDFT.addDFTComboTest(SRC_IMG_GREY, roi, "fft_lowpass", [12])
        trDFT.addDFTComboTest(SRC_IMG_GREY, roi, "fft_lowpass", [24])
        trDFT.addDFTComboTest(SRC_IMG_GREY, roi, "fft_lowpass", [36])

    # image, grey, lowpass vs smoothing, micro
    if S:
        roi = ROI(122,145,556,358)
        trDFT.addDFTComboTest(SRC_IMG_GREY, roi, "fft_lowpass", [14])
        trDFT.addDFTTest(SRC_IMG_GREY, roi, "smoothing", [8])

    # video, grey, lowpass, catdog
    if S:
        roi = ROI(72,51,188,145)
        trDFT.addDFTComboTest(SRC_VID_GREY2, roi, "fft_lowpass", [16], 
                    soi=(1,20), setGreyscaleVideo=True)

    # image, color, lowpass
    if S:
        roi = ROI(160,180,316,346)
        trDFT.addDFTTest(SRC_IMG_COLOR2, roi, "fft_lowpass", [20])

    # image, grey, highpass, micro
    if S:
        roi = ROI(122,145,556,358)
        for d0 in range(2, 50, 2):
            trDFT.addDFTComboTest(SRC_IMG_GREY, roi, "fft_highpass", [d0])

    # image, grey, highpass vs edgedetect, micro
    if S:
        roi = ROI(122,145,556,358)
        for d0 in range(0, 100, 2):
            trDFT.addDFTComboTest(SRC_IMG_GREY, roi, "fft_highpass", [d0])
        trDFT.addDFTTest(SRC_IMG_GREY, roi, "edgedetect", [71])

    # video, grey, highpass, catdog
    if S:
        roi = ROI(72,51,188,145)
        for d0 in range(0, 40, 2):
            trDFT.addDFTComboTest(SRC_VID_GREY2, roi, "fft_highpass", [d0], 
                    soi=(1,20), setGreyscaleVideo=True)

    # image, color, highpass
    if S:
        roi = ROI(160,180,316,346)
        for d0 in range(0, 60, 2):
            trDFT.addDFTComboTest(SRC_IMG_COLOR2, roi, "fft_highpass", [d0])

    # image, grey, bandpass, micro
    if S:
        roi = ROI(122,145,556,358)
        trDFT.addDFTComboTest(SRC_IMG_GREY, roi, "fft_bandpass", [0, 40])
        trDFT.addDFTComboTest(SRC_IMG_GREY, roi, "fft_bandpass", [2, 62])
        trDFT.addDFTComboTest(SRC_IMG_GREY, roi, "fft_bandpass", [10, 42])
        trDFT.addDFTComboTest(SRC_IMG_GREY, roi, "fft_bandpass", [20, 20])

    # video, color, bandpass
    if S:
        roi = ROI(30,193,323,241)
        trDFT.addDFTComboTest(SRC_VID_COLOR, roi, "fft_bandpass", [15, 60], 
                soi=(1,10))

    # image, grey, bandstop, micro
    if S:
        roi = ROI(122,145,556,358)
        trDFT.addDFTComboTest(SRC_IMG_GREY, roi, "fft_bandstop", [8, 20])
        trDFT.addDFTComboTest(SRC_IMG_GREY, roi, "fft_bandstop", [16, 40])
        trDFT.addDFTComboTest(SRC_IMG_GREY, roi, "fft_bandstop", [32, 80])
        trDFT.addDFTComboTest(SRC_IMG_GREY, roi, "fft_bandstop", [64, 160])

    # video, color, bandstop
    if S:
        roi = ROI(30,193,323,241)
        trDFT.addDFTComboTest(SRC_VID_COLOR, roi, "fft_bandstop", [30, 60], 
                soi=(1,10))

    # --- Extras ---

    if 0:
        roi = ROI(52,33,404,423)
        dMax = getMaxRadius(roi)
        for d0 in range(0, 100, 20):
            trDFT.addDFTTest(SRC_IMG_GREY2, roi, "fft_lowpass", [d0])

    if 0:
        roi = ROI(52,33,404,423)
        dMax = getMaxRadius(roi)
        for d0 in range(0, dMax, 20):
            trDFT.addDFTTest(SRC_IMG_GREY2, roi, "fft_highpass", [d0])

    if 0:
        roi = ROI(72,51,188,145)
        dMax = getMaxRadius(roi)
        for d0 in range(0, dMax, 20):
            trDFT.addDFTTest(SRC_IMG_COLOR, roi, "fft_lowpass", [d0])

    if 0:
        roi = ROI(72,51,188,145)
        dMax = getMaxRadius(roi)
        for d0 in range(0, dMax, 20):
            trDFT.addDFTTest(SRC_IMG_COLOR, roi, "fft_highpass", [d0])

    if 0:
        roi = ROI(122,145,553,357)
        dMax = getMaxRadius(roi)
        step = 20
        for d0 in range(0, dMax/2, step):
            for d1 in range(int(dMax/2.0), dMax, step):
                trDFT.addDFTTest(SRC_IMG_COLOR, roi, "fft_bandpass", [d0, d1])

    if 0:
        roi = ROI(122,145,553,357)
        dMax = getMaxRadius(roi)
        step = 20
        for d0 in range(0, dMax/2, step):
            for d1 in range(int(dMax/2.0), dMax, step):
                trDFT.addDFTTest(SRC_IMG_COLOR, roi, "fft_bandstop", [d0, d1])

    #trDFT.run()
    trDFT.save("test.sh")
    
