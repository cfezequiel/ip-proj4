#! /usr/bin/python
# Test generator

import copy
import os

PROGRAM="../bin/iptool"

class SOI:
    """Sequence of interest parameter."""

    def __init__(self, beg, end):
        self.beg = beg
        self.end = end

class ROI:
    """ Region of interest parameter."""

    def __init__(self, x, y, sx, sy):
        self.x = x
        self.y = y
        self.sx = sx
        self.sy = sy

class IPFilter:

    def __init__(self, name, params):
        self.name = name
        self.params = params

    def __str__(self):
        return self.name

class IPParam:
    """Represents a IP parameter."""

    def __init__(self, f, roi, soi=SOI(1,1)):
        self.f = f
        self.roi = roi
        self.soi = soi

class IPParams:
    """Represents IP parameters."""
    
    def __init__(self, filename, ipparams=[]):
        self.ipparams = ipparams
        self.filename = filename

    def save(self):
        fp = open(self.filename, "w")
        fp.write(str(len(self.ipparams)) + "\n")
        for p in self.ipparams:
            fp.write("%s %s %s %s " % (p.roi.x, p.roi.y, p.roi.sx, p.roi.sy))
            fp.write("%s %s " % (p.soi.beg, p.soi.end))
            fp.write("%s " % p.f)
            for filter_params in p.f.params:
                fp.write("%s " % filter_params)
            fp.write("\n")
        fp.close()

    def __str__(self):
        return self.filename

class IPTest:
    """ Represents an iptool test."""

    def __init__(self, src, tgt, p, opts=''):
        self.src = src 
        self.tgt = tgt
        self.p = p
        self.opts =opts

    def cmdstr(self):
        """Return command line string."""

        # Form command
        cmd = "%s %s %s %s %s" % (PROGRAM, self.opts, self.src, self.tgt, self.p)

        #time = "/usr/bin/time"
        time = "time"
        s = 'echo "' + cmd + '"\n'
        #s += time + ' --format="%E" ' + cmd
        s += time +  ' ' + cmd
        return s

    def run(self):
        """Run this test."""

        self.p.save()
        cmd = self.cmdstr()
        status = os.system(cmd)
        if (status):
            print "FAILED! -> %s" % cmd

    def save(self, fp):
        """Save test to file."""

        # Save parameter file
        self.p.save()

        # Save shell command
        fp.write(self.cmdstr()+ '\n')

    def __str__(self):
        s = "IPTest: src=%s, tgt=%s, param=%s" % (self.src, self.tgt, self.p)
        if self.opts:
            s += " opts=%s" % self.opts
        return s

class IPTestRunner:
    """Executes a set of IP tests."""

    def __init__(self, tests=[]):
        self.tests = tests
        self.setupCommands = []
        self.tearDownCommands = []

    def run(self):
        """Execute test."""
        
        for cmd in self.setupCommands:
            print cmd
            status = os.system(cmd)
            if status: 
                print 'Error! Code = %d' % status
            
        for test in self.tests:
            test.run()

        for cmd in self.tearDownCommands:
            print cmd
            status = os.system(cmd)
            if status: 
                print 'Error! Code = %d' % status

    def add(self, test):
        self.tests.append(test)

    def addSetupCmd(self, cmd):
        """Add a command to be executed before the tests."""

        self.setupCommands.append(cmd)

    def addTearDownCmd(self, cmd):
        """Add a command to be executed after the tests."""

        self.tearDownCommands.append(cmd)

    def clear(self):
        """Remove all tests."""
        self.tests = []

    def save(self, filename, mode='w'):
        """Save test as shell script."""

        fp = open(filename, mode)
        for cmd in self.setupCommands:
            fp.write('echo "%s"\n' % cmd)
            fp.write(cmd + '\n')

        for test in self.tests:
            test.save(fp)

        for cmd in self.tearDownCommands:
            fp.write('echo "%s"\n' % cmd)
            fp.write(cmd + '\n')

        fp.close()

        os.system('chmod 744 %s' % filename)


