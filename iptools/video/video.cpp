#include <assert.h>
#include <dirent.h>
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <algorithm>
#include "../common.h"
#include "video.h"
using namespace std;

int runcmd(const stringstream &cmd)
{
    string cmdstr(cmd.str());
    return system(cmdstr.c_str());
}

bool FrameCompare(Frame *left, Frame *right)
{
    cout << "left = " << left << " right = " << right << endl;
    if (!left || !right)
    {
        return false;
    }

    return (*left).path.compare((*right).path);
}

Frame::Frame(string path, int seqNum)
{
    Frame::assign(path, seqNum);
}

void Frame::assign(string path, int seqNum)
{
    this->path = path;
    this->seqNum = seqNum;
}

Video::Video()
{
    // do nothing
}

Video::~Video()
{
    this->clear();
}

Video::Video(string filename, COLORSPACE cs)
{
    this->loadVideo(filename, cs);
}

void Video::clear()
{
    vector<Frame *>::iterator iter;
    for (iter = _frames.begin(); iter != _frames.end(); iter++)
    {
        delete *iter;
    }
    _frames.clear();
}

void Video::loadVideo(string filename, COLORSPACE cs)
{
    int err;
    string ext;
    stringstream cmd;
    DIR *dp;
    struct dirent *de;

    // Clear Frame buffer if not empty
    if (!_frames.empty())
    {
        this->clear();
    }

    // Set image file extension
    if (cs == CS_GREY)
    {
        ext.assign("pgm");
    }
    else if (cs == CS_RGB || cs == CS_HSI)
    {
        ext.assign("ppm");
    }

    // Generate temporary directory to store video frames
    stringstream frameDir;
    frameDir << "tmp_" << rand();
    frameDir >> this->_frameDir;
    cmd << "mkdir " << this->_frameDir;
    err = runcmd(cmd);
    if (err)
    {
        throw IPException("Could not create frame directory.");
    }

    // Extract frames from video file into Frame directory using ffmpeg
    stringstream ss;
    ss << "img%03d." << ext;
    cmd.str(string());
    cmd.clear();
    cmd << "ffmpeg -v error -i " << filename << ' ' << this->_frameDir << '/' <<
        ss.str();
    err = runcmd(cmd);
    if (err)
    {
        throw IPException("Failed to extract Video Frames.");
    }
    this->_frameFileFormat = ss.str();

    // Open frame directory
    dp = opendir(this->_frameDir.c_str());
    if (dp == NULL)
    {
        throw IPException("Failed to open Frame directiory.");
    }

    // Read frame names and store in a container
    vector<string> framePaths;
    while (true)
    {
        de = readdir(dp);
        if (de == NULL)
        {
            break;
        }
        if (!strcmp(de->d_name, ".") || !strcmp(de->d_name, ".."))
        {
            continue;
        }

        framePaths.push_back(string(de->d_name));
    }

    // Close directory
    closedir(dp);

    // Sort frame names
    sort(framePaths.begin(), framePaths.end());

    // Read frame data 
    for (unsigned int i = 0; i < framePaths.size(); i++)
    {
        // Assemble full path to Frame file
        stringstream framePath;
        framePath << this->_frameDir << '/' << framePaths[i];
        string name(framePath.str());

        // Read Frame file
        Frame *frame = new Frame(name, i + 1);
        Image *img = load(name.c_str(), cs);
        frame->copy((Frame &) *img);
        _frames.push_back(frame);
    }
}

void Video::saveVideo(string filename)
{
    // Make sure that the 'load' function was used properly
    if (_frameDir.empty() || _frames.empty())
    {
        throw IPException("No frames to save.");
    }

    // Save each frame to corresponding frame file
    for (int i = 0; i < getFrameCount(); i++)
    {
        save((const Image &) *_frames[i], _frames[i]->path.c_str());
    }

    // Compile all frames in frame directory 
    stringstream cmd;
    cmd << "ffmpeg -y -v quiet -i " << this->_frameDir << '/' <<
        this->_frameFileFormat << ' ' << filename;
    int err = runcmd(cmd);
    if (err)
    {
        throw IPException("Could not generate Frames from vidoe.");
    }

}

void Video::close()
{
    if (this->_frameDir.empty())
    {
        throw IPException("Video was not open.");
    }

    // Remove temporary frame directory
    stringstream cmd;
    cmd << "rm -r " << this->_frameDir;
    int err = runcmd(cmd);
    if (err)
    {
        throw IPException("Unable to remove temporary Frame directory.");
    }
}

int Video::getFrameCount()
{
    return _frames.size();
}

Frame * Video::getFrame(unsigned int i)
{
    return _frames[i];
}

void Video::setFrameData(unsigned int i, Image &img)
{
    _frames[i]->copy(img);
}



