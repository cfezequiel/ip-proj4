#ifndef VIDEO_H
#define VIDEO_H

#include <vector>
#include <string>
#include "../image/image.h"

class Frame: public Image
{
    public:
        int seqNum;
        string path;

        Frame() : Image() {}
        Frame(string path, int seqNum);
        Frame(const Frame &f) {}
        void assign(string path, int seqNum);
};

class Video 
{
	private: 
        string _frameFileFormat;
        string _frameDir;
        vector<Frame *> _frames;

	public: 

		Video();
        Video(string filename, COLORSPACE cs=CS_HSI);
		~Video();

        void loadVideo(string filename, COLORSPACE cs=CS_HSI);
        void saveVideo(string filename);
        void clear();
        void close();
        int getFrameCount();
        Frame * getFrame(unsigned int i);
        void setFrameData(unsigned int i, Image &img);
};

#endif

