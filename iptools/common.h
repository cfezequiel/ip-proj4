#ifndef COMMON_H
#define COMMON_H
#include <stdexcept>

#ifdef DEBUG
#include <stdio.h>
#endif

class IPException: public std::runtime_error
{
    public:
        IPException(const char *m) : std::runtime_error(m) {};
};

#endif //COMMON_H

