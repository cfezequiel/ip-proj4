#ifndef IMAGE_H
#define IMAGE_H
#include <iostream>
#include <stdio.h>
#include <stdlib.h> 
#include <string>
#include <vector>

using namespace std;

#define PI        3.14159265358979f
#define G_RANGE   256
#define G_MAX     255
#define RGB_MAX   G_MAX
#define H_MAX     360
#define S_MAX     100
#define I_MAX     255
#define RGB_RANGE G_RANGE
#define NCHANNELS 3

enum RGBCOLOR
{
    RGBCOLOR_GREY    = 0x999999,
    RGBCOLOR_RED     = 0xff0000,
    RGBCOLOR_GREEN   = 0x00ff00,
    RGBCOLOR_BLUE    = 0x0000ff,
    RGBCOLOR_YELLOW  = 0xffff00,
    RGBCOLOR_CYAN    = 0x00ffff,
    RGBCOLOR_MAGENTA = 0xff00ff
};

enum HSICOLOR
{
    HSICOLOR_YELLOW = 60
};

enum RGBCHANNEL
{
    GREY  = 0x0,
    RED   = 0x1, 
    GREEN = 0x2, 
    BLUE  = 0x4
};

enum HSICHANNEL 
{
    HUE         = 0x1, 
    SATURATION  = 0x2, 
    INTENSITY   = 0x4
}; 

enum COLORSPACE
{
    CS_GREY = 0,
    CS_RGB  = 1,
    CS_HSI  = 2,
    CS_UNDEFINED = 3
};

enum IMAGE_FORMAT
{
    IMG_FMT_PGM = 0,
    IMG_FMT_PPM = 1
};

class Image
{
    public:
        Image() :_numRows(0), _numColumns(0) {};
        Image(COLORSPACE cs);
        Image(const Image &img);
        Image(int rows, int columns);

        COLORSPACE getColorspace() const {return _cs;}
        void clear();
        void copy(const Image &img);
        void resize(int rows, int cols);
        void setPixel(int row, int col, int channel, int value);
        void setPixel(int row, int col, int color);
        int getPixel(int row, int col, int channel) const;
        int rows() const;
        int columns() const;

        vector<int> getChannel(int channel) const;
        void setChannel(int ch, const vector<int> &src);   

        bool empty();
        int maxval(int ch);

    protected:
        COLORSPACE _cs;
        int _numRows;
        int _numColumns; 
        vector<int> _channels[NCHANNELS];
};

// Utilities
void save(const Image &img, const char *file);
Image * load(const char *file, COLORSPACE cs=CS_RGB);
void rgb2hsi(int r, int g, int b, double *h, double *s, double *i);
void hsi2rgb(double h, double s, double i, int *r, int *g, int *b);

#endif //IMAGE_H

