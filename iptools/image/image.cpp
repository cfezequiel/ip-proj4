#include <cassert>
#include <cmath>
#include <string.h>
#include <float.h>
#include <limits.h>
#include "common.h"
#include "../ipparams/ipparams.h"
#include "image.h"

//-------------------- Image --------------------  
Image::Image(COLORSPACE cs)
{
    _cs = cs;
    _numRows = 0;
    _numColumns = 0;
}

Image::Image(const Image &img)
{
	copy(img);
}

Image::Image(int rows, int columns) 
{
	resize(rows, columns);
}

int Image::rows() const
{
    return _numRows;
}

int Image::columns() const
{
    return _numColumns;
}

void Image::clear() 
{
    _numRows = _numColumns = 0;
    for (int i = 0; i < NCHANNELS; i++)
    {
        _channels[i].clear();
    }
}

void Image::copy(const Image &img) 
{
    _cs = img.getColorspace();
	resize(img.rows(), img.columns());
    setChannel(0, img.getChannel(0));
    setChannel(1, img.getChannel(1));
    setChannel(2, img.getChannel(2));
}

void Image::resize(int rows, int columns)
{
	int length = rows*columns;
	_numRows = rows;
	_numColumns = columns;

    for (int i = 0; i < NCHANNELS; i++)
    {
        _channels[i].clear();
        _channels[i].resize(length, 0);
    }
}

void Image::setPixel(int row, int col, int ch, int value)
{
    _channels[ch][row * _numColumns + col] = value;
}

void Image::setPixel(int row, int col, int color)
{
    if (getColorspace() == CS_GREY)
    {
        setPixel(row, col, 0, color);
    }
    else if (getColorspace() == CS_RGB)
    {
        int r,g,b;
        b = color & 0xff;
        g = (color >> 8) & 0xff;
        r = (color >> 16) & 0xff;

        setPixel(row, col, 0, r);
        setPixel(row, col, 1, g);
        setPixel(row, col, 2, b);
    }
    else if (getColorspace() == CS_HSI)
    {
        setPixel(row, col, 0, color);
        setPixel(row, col, 1, 100);
        setPixel(row, col, 2, 255);
    }
}

int Image::getPixel(int row, int col, int ch) const
{
    return _channels[ch][row * _numColumns + col];
}

vector<int> Image::getChannel(int ch) const
{
    return _channels[ch];
}

void Image::setChannel(int ch, const vector<int> &channel)
{
    if (channel.size() != _channels[ch].size())
    {
        throw IPException("Cannot set channel. Incompatible size.");
    }
    _channels[ch] = channel;
}

bool Image::empty() 
{
    return !_numRows;
}

int Image::maxval(int ch)
{
    //FIXME: for now, just set to RGB_MAX; later, support H and S channel ranges
    return RGB_MAX;
}

//-------------------- end Image --------------------  

int getint(FILE *fp) 
{
    int item, i, flag;

    /* skip forward to start of next number */
    item  = getc(fp); 
    flag = 1;
    do {
        if (item =='#') 
        {   /* comment*/
            while (item != '\n' && item != EOF) item=getc(fp);
        }

        if (item ==EOF) return 0;
        if (item >='0' && item <='9') 
            {flag = 0;  break;}

        /* illegal values */
        if ( item !='\t' && item !='\r' 
            && item !='\n' && item !=',') return(-1);

        item = getc(fp);
    } while (flag == 1);


    /* get the number*/
    i = 0; 
    flag = 1;
    do 
    {
        i = (i*10) + (item - '0');
        item = getc(fp);
        if (item <'0' || item >'9') 
        {
            flag = 0; break;
        }
        if (item==EOF) 
            break; 
        
    } while (flag == 1);

    return i;
}


//-------------------- Utilities --------------------  
void save(const Image &img, const char* file)
{
	FILE *outfile;
	IMAGE_FORMAT fmt;

    // Open file for writing
	if ((outfile = fopen(file, "w")) == NULL) 
	{
		printf("Cannot open file, %s for writing.\n", file) ;
        throw IPException("File error.");
	}

	fmt = strstr(file, ".ppm") != NULL ? IMG_FMT_PPM: IMG_FMT_PGM;

	if (fmt == IMG_FMT_PGM)
    {
        int g;

        // Write PGM header
		fprintf(outfile, "P5\n%d %d\n255\n", img.columns(), img.rows());

		for (int i=0; i < img.rows(); i++)
		{
            for (int j = 0; j < img.columns(); j++)
            {
                g = img.getPixel(i, j, 0);
                g = g > G_MAX ? G_MAX : g;
                putc((unsigned char) g, outfile);
            }
		}
    }
	else if (fmt == IMG_FMT_PPM)
    {
        int r,g,b;

        // Write PPM header
        fprintf(outfile, "P6\n%d %d\n255\n", img.columns(), img.rows());

        if (img.getColorspace() == CS_RGB)
        {
            for (int i=0; i < img.rows(); i++)
            {
                for (int j = 0; j < img.columns(); j++)
                {
                    r = img.getPixel(i, j, 0);
                    g = img.getPixel(i, j, 1);
                    b = img.getPixel(i, j, 2);

                    r = r > RGB_MAX ? RGB_MAX: r;
                    g = g > RGB_MAX ? RGB_MAX: g;
                    b = b > RGB_MAX ? RGB_MAX: b;

                    putc((unsigned char) r, outfile);
                    putc((unsigned char) g, outfile);
                    putc((unsigned char) b, outfile);
                }
            }
        }
        else if (img.getColorspace() == CS_HSI)
        {
            double h,s,i;
            for (int y=0; y < img.rows(); y++)
            {
                for (int x = 0; x < img.columns(); x++)
                {
                    h = img.getPixel(y, x, 0);
                    s = img.getPixel(y, x, 1);
                    i = img.getPixel(y, x, 2);
                    h = h > H_MAX ? H_MAX : h;
                    s = s > S_MAX ? S_MAX : s;
                    i = i > I_MAX ? I_MAX : i;

                    hsi2rgb(h, s, i, &r, &g, &b);

                    r = r > RGB_MAX ? RGB_MAX: r;
                    g = g > RGB_MAX ? RGB_MAX: g;
                    b = b > RGB_MAX ? RGB_MAX: b;

                    putc((unsigned char) r, outfile);
                    putc((unsigned char) g, outfile);
                    putc((unsigned char) b, outfile);
                }
            }
        }
    }
    else
    {
        IPException("Unrecognized image format.");
    }

	fclose(outfile);
}

Image *load(const char *file, COLORSPACE cs) 
{
	FILE *fp;
	char str[50];
    IMAGE_FORMAT fmt;

	if (strstr(file, ".ppm") != NULL) {/* PPM Color File*/
		fmt = IMG_FMT_PPM;
	}
    else
    {
        fmt = IMG_FMT_PGM;
    }

    // Open file
	if ((fp = fopen(file, "r")) == NULL) {
		fprintf(stderr, "\nCannot open image file %s\n", file);
		exit(1);
	}

    // Check file format if supported
	fscanf(fp, "%s", str) ;
	if ((fmt == IMG_FMT_PPM) && (strcmp (str, "P6") != 0)) {
		fprintf(stderr, "\n image file %s not in PPM format...%s", file, str);
		return false;
	}
	if ((fmt == IMG_FMT_PGM) && (strcmp (str, "P5") != 0)) {
		fprintf(stderr, "\n image file %s not in PGM format...%s", file, str);
		return false;
	}

    // Create new image 
    Image *img;
    if (fmt == IMG_FMT_PGM)
    {
        img = new Image(CS_GREY);
    }
    else if (fmt == IMG_FMT_PPM)
    {
        if (cs == CS_RGB)
        {
            img = new Image(CS_RGB);
        }
        else if (cs == CS_HSI)
        {
            img = new Image(CS_HSI);
        }
    }

    // Get image dimensions
    int cols = getint(fp);
	while (cols == -1 || cols == 0)
    {
		cols = getint(fp);
    }

    int rows = getint(fp);
	while (rows == -1 || rows == 0)
    {
		rows = getint(fp);
    }

	img->resize(rows, cols);

    int dummy;
	fscanf(fp, "%d", &dummy);  
	(getc(fp)); /* gets the carriage return*/

	if (fmt == IMG_FMT_PGM)
	{
        int g;
        for (int i = 0; i < img->rows(); i++)
        {
            for (int j = 0; j < img->columns(); j++)
            {
                g = (unsigned char) (getc(fp));  
                img->setPixel(i, j, 0, g);
            }
		}
	}
	else if (fmt == IMG_FMT_PPM)
	{
        if (cs == CS_RGB)
        {
            unsigned char r, g, b;
            for (int i = 0; i < img->rows(); i++)
            {
                for (int j = 0; j < img->columns(); j++)
                {
                    r = (unsigned char) (getc(fp));  
                    g = (unsigned char) (getc(fp));  
                    b = (unsigned char) (getc(fp));  
                    img->setPixel(i, j, 0, r);
                    img->setPixel(i, j, 1, g);
                    img->setPixel(i, j, 2, b);
                }
            }
        }
        else if (cs == CS_HSI)
        {
            int r,g,b;
            double h, s, i;
            for (int y = 0; y < img->rows(); y++)
            {
                for (int x =0; x < img->columns(); x++)
                {
                    r = getc(fp);  
                    g = getc(fp);  
                    b = getc(fp);  
                    r = r > RGB_MAX ? RGB_MAX: r;
                    g = g > RGB_MAX ? RGB_MAX: g;
                    b = b > RGB_MAX ? RGB_MAX: b;

                    rgb2hsi(r, g, b, &h, &s, &i);

                    img->setPixel(y, x, 0, (int) h);
                    img->setPixel(y, x, 1, (int) s);
                    img->setPixel(y, x, 2, (int) i);
                }
            }
        }
        else
        {
            throw IPException("Invalid colorspace specification.");
        }
	} 
    else
    {
        throw IPException("Unrecognized file format.");
    }

	fclose(fp);

    return img;
}

double min(double a, double b, double c)
{
    if (a < b)
    {
        if (a < c)
        {
            return a;
        }
    }
    else
    {
        if (b < c)
        {
            return b;
        }
    }

    return c;
}

void rgb2hsi(int r, int g, int b, double *h, double *s, double *i)
{
    assert(r <= RGB_MAX);
    assert(g <= RGB_MAX);
    assert(b <= RGB_MAX);

    int n;
    double rn,gn,bn;
    double hn,sn,in;
    double t;

    n = r + g + b; 
    rn = (double) r/n;
    gn = (double) g/n;
    bn = (double) b/n;

    t = (rn - gn) * (rn - gn) + (rn - bn) * (gn - bn);
    if (t == 0)
    {
        hn = 0;
    }
    else
    {
        t = 0.5 * ((rn - gn) + (rn - bn)) / sqrt(t);
        if (bn <= gn)
        {
            hn = acos(t);
        }
        else
        {
            hn = 2 * PI - acos(t);
        }
    }

    sn = 1 - 3 * min(rn, gn, bn);
    in = ((double) r + g + b) / (NCHANNELS * RGB_MAX);

    // Convert to integral ranges for output
    *h = (hn * 180 / PI);
    *s = (sn * 100);
    *i = (in * 255);

    // H, S, I must be within their respective ranges
    if (*h > H_MAX)
    {
        *h = H_MAX;
    }
    if (*s > S_MAX)
    {
        *s = S_MAX;
    }
    if (*i > I_MAX)
    {
        *i = I_MAX;
    }
    if (*h < 0)
    {
        *h = 0;
    }
    if (*s < 0)
    {
        *s = 0;
    }
    if (*i < 0)
    {
        *i = 0;
    }
}

void hsi2rgb(double h, double s, double i, int *r, int *g, int *b)
{
    assert(h <= H_MAX);
    assert(s <= S_MAX);
    assert(i <= I_MAX);

    double h_, s_, i_;
    double x=0, y=0, z=0;
    double *pr; 
    double *pg;
    double *pb;

    // Convert to normalized ranges
    h_ = (double) h * PI / 180;
    s_ = (double) s / 100;
    i_ = (double) i / 255;

    if (h_ >= 2 * PI)
    {
        h_ = 0;
    }

    if (h_ < 2 * PI / 3)
    {
        pr = &y;
        pg = &z;
        pb = &x;
    }
    else if (2 * PI / 3 <= h_  && h_ < 4 * PI / 3)
    {
        h_ = h_ - (2 * PI / 3);
        pr = &x;
        pg = &y;
        pb = &z;
    }
    else if (4 * PI / 3 <= h_ && h_ < 2 * PI)
    {
        h_ = h_ - (4 * PI / 3);
        pr = &z;
        pg = &x;
        pb = &y;
    }

    x = i_ * (1 - s_);
    y = i_ * (1 + (s_ * cos(h_)) / cos(PI / 3 - h_));
    z = 3 * i_ - (x + y);

    // Normalized r,g,b values must be [0,1]
    if (*pr > 1)
    {
        *pr = 1;
    }
    if (*pg > 1)
    {
        *pg = 1;
    }
    if (*pb > 1)
    {
        *pb = 1;
    }

    *r = (int) round(*pr * RGB_MAX);
    *g = (int) round(*pg * RGB_MAX);
    *b = (int) round(*pb * RGB_MAX);
}


