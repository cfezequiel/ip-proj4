#ifndef UTILITY_H
#define UTILITY_H

#include <string>
#include <fftw3.h>
#include "../ipparams/ipparams.h"
#include "../video/video.h"
#include "../image/image.h"

enum VID_MODE
{
    PER_FRAME = 0,
    PER_SOI = 1
};

class utility
{
	public:
		utility();
		virtual ~utility();
        static void addGrey(Image &src, Image &tgt, ROI roi, int value);
        static void processImageFFT(Image &src, Image &tgt, IPParam param);
        static void processImage(Image &src, Image &tgt, IPParam param);
        static void processImage(string srcPath, string tgtPath, vector<IPParam> params);
        static void processVideo(string srcPath, string tgtPath, vector<IPParam> ps,
                    COLORSPACE cs, VID_MODE mode);
};

string getext(string path);

#endif

