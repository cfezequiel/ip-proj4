#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <strings.h>
#include <time.h>
#include <vector>
#include <cmath>
#include <sstream>
#include "common.h"
#include "utility.h"
using namespace std;

#define USE_COMPLEX_AMPLITUDE_DISPLAY 0
#define SWAP_QUADRANTS 1
#define PRESERVE_PHASE 0
#define STRLEN 256
#define ZERO_OUT_NEGATIVE 1

/**
 * Get file extension
 */
string getext(string path)
{
    int dotPos = path.find_last_of(".");

    // Get extension without the "."
    string ext = path.substr(dotPos + 1);

    return ext;
}

/**
 * Checks if ROI is within an image
 */
bool roiInImage(Image &img, ROI roi)
{
    int xmax;
    int ymax;
    int imgWidth = img.columns();
    int imgHeight = img.rows();

    xmax = roi.x + roi.sx;
    ymax = roi.y + roi.sy;

    if (roi.x < 0 || roi.y < 0 || xmax > imgWidth || ymax > imgHeight)
    {
        return false;
    }

    return true;
}

/**
 * Extracts an image within ROI of a source image
 */
Image * select(Image &img, ROI roi)
{
    if (!roiInImage(img, roi))
    {
        throw IPException("ROI is not within image.");
    }

    Image *imgInROI = new Image(roi.sy, roi.sx);
    for (int i = roi.y; i < roi.y + roi.sy; i++)
    {
        for (int j = roi.x; j < roi.x + roi.sx; j++)
        {
            for (int k = 0; k < NCHANNELS; k++)
            {
                int pixel = img.getPixel(i, j, k);
                imgInROI->setPixel(i - roi.y, j - roi.x, k, pixel);
            }
        }
    }
    return imgInROI;
}

// Copy source image on top of target image at the given ROI
// The copy starts from coordinate (0,0) of the source image, until the x,y-sizes of the ROI
void Overlay(Image &src, Image &target, ROI roi)
{
    //TODO
}

void clearImageROIChannel(Image &img, ROI roi, int channel)
{
    for (int i = roi.y; i < roi.y + roi.sy; i++)
    {
        for (int j = roi.x; j < roi.x + roi.sx; j++)
        {
            img.setPixel(i, j, channel, 0);
        }
    }
}

void swapAndMirrorImageQuandrants(Image &img, ROI roi, int channel)
{
    int nColsHalf = roi.sx/2;
    int nRowsHalf = roi.sy/2;
    int ip, jp;
    int pixel;

    // Move halves
    for (int i = roi.y; i < roi.y + roi.sy; i++)
    {
        for (int j = roi.x; j < roi.x + nColsHalf; j++)
        {
            if (i < roi.y + nRowsHalf)
            {
                //Move top-left to bottom-right
                ip = nRowsHalf + i;
                jp = nColsHalf + j;
            }
            else
            {
                //Move bottom-left to top right
                ip = i - nRowsHalf;
                jp = j + nColsHalf;
            }
            pixel = img.getPixel(i, j, channel);
            img.setPixel(ip, jp, channel, pixel);
        }
    }

    // Apply diagonal symmetry
    int yc = roi.y + nRowsHalf;
    int xc = roi.x + nColsHalf;
    for (int i = roi.y; i < roi.y + roi.sy; i++)
    {
        for (int j = roi.x + nColsHalf; j < roi.x + roi.sx; j++)
        {
            ip = 2 * yc - i - 1;
            jp = 2 * xc - j - 1;
            pixel = img.getPixel(i, j, channel);
            img.setPixel(ip, jp, channel, pixel);
        }
    }
}

void scaleFFT(fftw_complex *in, fftw_complex *out, size_t size, double scale)
{
    assert(in != NULL);
    assert(out != NULL);

    for (unsigned int k = 0; k < size; k++)
    {
        out[k][0] = in[k][0] * scale;
        out[k][1] = in[k][1] * scale;
    }
}

void swapImageQuandrants(Image &img, ROI roi, int channel)
{
    int halfSizeX = roi.sx/2;
    int halfSizeY = roi.sy/2;
    int upper, lower;

    for (int i = roi.y; i < roi.y + halfSizeY; i++)
    {
        for (int j = roi.x; j < roi.x + halfSizeX; j++)
        {
            // Swap top-left and bottom-right
            upper = img.getPixel(i, j, channel);
            lower = img.getPixel(i + halfSizeY, j + halfSizeX, channel);
            img.setPixel(i, j, channel, lower);
            img.setPixel(i + halfSizeY, j + halfSizeX, channel, upper); 

            // Swap top-right and bottom-left
            upper = img.getPixel(i, j + halfSizeX, channel);
            lower = img.getPixel(i + halfSizeY, j, channel);
            img.setPixel(i, j + halfSizeX, channel, lower);
            img.setPixel(i + halfSizeY, j, channel, upper); 
        }
    }
}

fftw_complex * image2fft(Image &src, ROI roi, int channel)
{
    // Set dimensions of region to process
    int colBeg = roi.x;
    int rowBeg = roi.y;
    int nRows = roi.sy;
    int nCols = roi.sx;

    double *in = (double *) malloc(sizeof(double) * nRows * nCols);

    // Store image data into FFTW array 
    int k;
    for (int i = rowBeg; i < rowBeg + nRows; i++)
    {
        for (int j = colBeg; j < colBeg + nCols; j++)
        {
            k = (j - colBeg) + nCols * (i - rowBeg);
            in[k] = src.getPixel(i, j, channel); //real part
        }
    }

    // Allocate memory for output FFT array
    fftw_complex *out = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * nRows * nCols);

    // Create plan
    fftw_plan p = fftw_plan_dft_r2c_2d(nRows, nCols, in, out, FFTW_ESTIMATE);

    // -- Execute plan
    fftw_execute(p);

    // -- Destroy plan
    fftw_destroy_plan(p);

    // Delete temporary variable
    delete in;

    return out;
}

void fft2image(fftw_complex *in, int size, Image &tgt, ROI roi, int channel)
{
    // Set dimensions of region to process
    int colBeg = roi.x;
    int rowBeg = roi.y;
    int nRows = roi.sy;
    int nCols = roi.sx;

    // Check that the size of the fftw data is correct with respect to output
    // image ROI
    assert(size == nRows * nCols);

    // Create temporary storage for IFFT output
    double *out = (double *) malloc(sizeof(double) * nRows * nCols);

    // Create plan for IFFT
    fftw_plan p = fftw_plan_dft_c2r_2d(nRows, nCols, in, out, FFTW_ESTIMATE);

    // Execute plan
    fftw_execute(p);

    // Destroy plan
    fftw_destroy_plan(p);

    // Copy converted real values to image
    int k;
    int pixel;
    for (int i = rowBeg; i < rowBeg + nRows; i++)
    {
        for (int j = colBeg; j < colBeg + nCols; j++)
        {
            k = (j - colBeg) + nCols * (i - rowBeg);
#if ZERO_OUT_NEGATIVE
            pixel = (int) (out[k] < 0 ? 0.0 : out[k]);
#endif
            tgt.setPixel(i, j, channel, pixel);
        }
    }

    // Delete temporary variable
    free(out);
}

void displayFFTAmplitudeImage2(fftw_complex *in, int size, Image &tgt, ROI roi, int channel)
{
    assert(in != NULL);

    // Set dimensions of region to process
    int colBeg = roi.x;
    int rowBeg = roi.y;
    int nRows = roi.sy;
    int nCols = roi.sx;

    // Check that the size of the fftw data is correct with respect to output
    // image ROI
    assert(size == nRows * nCols);

    // Normalize FFT output by multiplying by a scaling factor
    double scale = tgt.maxval(channel) / (double) (nRows * nCols);
    scaleFFT(in, in, nRows * nCols, scale);

    // Copy FFT amplitude values to image
    int k;
    double amplitude, re, im;
    int nColsHalf = nCols / 2 + 1;
    for (int i = rowBeg; i < rowBeg + nRows; i++)
    {
        for (int j = colBeg; j < colBeg + nColsHalf; j++)
        {
            k = (j - colBeg) + nColsHalf * (i - rowBeg);
            re = in[k][0];
            im = in[k][1];
            amplitude = sqrt(re * re + im * im);
            tgt.setPixel(i, j, channel, (int) amplitude);
        }
    }

    // Swap quadrants
#if SWAP_QUADRANTS
    swapAndMirrorImageQuandrants(tgt, roi, channel);
#endif
}
void displayFFTAmplitudeImage(fftw_complex *in, int size, Image &tgt, ROI roi, int channel)
{
    assert(in != NULL);

    // Set dimensions of region to process
    int colBeg = roi.x;
    int rowBeg = roi.y;
    int nRows = roi.sy;
    int nCols = roi.sx;

    // Check that the size of the fftw data is correct with respect to output
    // image ROI
    assert(size == nRows * nCols);

    // Normalize FFT output by multiplying by a scaling factor
    double scale =  1.0 / (double) (nRows * nCols);
    scaleFFT(in, in, nRows * nCols, scale);

    // Inverse FFT complex-to-real
    double *outR = (double *) malloc(sizeof(double) * size);
    fftw_plan p1 = fftw_plan_dft_c2r_2d(nRows, nCols, in, outR, FFTW_ESTIMATE);
    fftw_execute(p1);
    fftw_destroy_plan(p1);

    // Copy real values to complex data structure
    fftw_complex *inC= (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * nRows * nCols);
    for (int i = 0; i < size; i++)
    {
        inC[i][0] = outR[i];
        inC[i][1] = 0;
    }
    free(outR);

    // Forward FFT complex-to-complex
    fftw_plan p2 = fftw_plan_dft_2d(nRows, nCols, inC, in, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(p2);
    fftw_destroy_plan(p2);
    free(inC);

    // Normalize FFT output by multiplying by a scaling factor
    scale = tgt.maxval(channel) / (double) (nRows * nCols);
    scaleFFT(in, in, nRows * nCols, scale);

    // Copy FFT amplitude values to image
    int k;
    double amplitude, re, im;
    for (int i = rowBeg; i < rowBeg + nRows; i++)
    {
        for (int j = colBeg; j < colBeg + nCols; j++)
        {
            k = (j - colBeg) + nCols * (i - rowBeg);
            re = in[k][0];
            im = in[k][1];
            amplitude = sqrt(re * re + im * im);
            tgt.setPixel(i, j, channel, (int) amplitude);
        }
    }

    // Swap quadrants
    swapImageQuandrants(tgt, roi, channel);
}

double fftGetDistanceFromOrigin(int i, int j, int nRows, int nCols)
{
    assert(i >= 0);
    assert(j >= 0);
    assert(nRows > 0);
    assert(nCols > 0);
    assert(j < nCols / 2 + 1);

    double r;
    int nRowsHalf = nRows / 2 + 1;

    if (i >= 0 && i < nRowsHalf)
    {
        // Quadrant 1
        r = sqrt(i * i + j * j);
    }
    else
    {
        // Quadrant 2
        r = sqrt((i - nRows + 1) * (i - nRows + 1) + j * j);
    }

    return r;
}


/*-----------------------------------------------------------------------**/
void fftLowPass(fftw_complex *in, fftw_complex *out, int nRows, int nCols, int d0)
{
    int k;
    double r;

    int nColsHalf = nCols / 2 + 1;

    for (int i = 0; i < nRows; i++)
    {
        for (int j = 0; j < nColsHalf; j++)
        {
            r = fftGetDistanceFromOrigin(i, j, nRows, nCols);
            k = j + nColsHalf * i;
            if (r <= d0)
            {
                out[k][0] = in[k][0];
                out[k][1] = in[k][1];
            }
            else
            {
                out[k][0] = 0;
#if PRESERVE_PHASE
                out[k][1] = in[k][1];
#else
                out[k][1] = 0;
#endif
            }
        }
    }
}

/*-----------------------------------------------------------------------**/
void fftHighPass(fftw_complex *in, fftw_complex *out, int nRows, int nCols, int d0)
{
    int k;
    double r;

    int nColsHalf = nCols / 2 + 1;

    for (int i = 0; i < nRows; i++)
    {
        for (int j = 0; j < nColsHalf; j++)
        {
            r = fftGetDistanceFromOrigin(i, j, nRows, nCols);
            k = j + nColsHalf * i;
            if (r >= d0)
            {
                out[k][0] = in[k][0];
                out[k][1] = in[k][1];
            }
            else
            {
                out[k][0] = 0;
#if PRESERVE_PHASE
                out[k][1] = in[k][1];
#else
                out[k][1] = 0;
#endif
            }
        }
    }
}

/*-----------------------------------------------------------------------**/
void fftBandPass(fftw_complex *in, fftw_complex *out, int nRows, int nCols, int d0, int d1)
{
    int k;
    double r;

    int nColsHalf = nCols / 2 + 1;

    for (int i = 0; i < nRows; i++)
    {
        for (int j = 0; j < nColsHalf; j++)
        {
            r = fftGetDistanceFromOrigin(i, j, nRows, nCols);
            k = j + nColsHalf * i;
            if (r >= d0 && r <= d1)
            {
                out[k][0] = in[k][0];
                out[k][1] = in[k][1];
            }
            else
            {
                out[k][0] = 0;
#if PRESERVE_PHASE
                out[k][1] = in[k][1];
#else
                out[k][1] = 0;
#endif
            }
        }
    }
}

void fftBandStop(fftw_complex *in, fftw_complex *out, int nRows, int nCols, int d0, int d1)
{
    int k;
    double r;

    int nColsHalf = nCols / 2 + 1;

    for (int i = 0; i < nRows; i++)
    {
        for (int j = 0; j < nColsHalf; j++)
        {
            r = fftGetDistanceFromOrigin(i, j, nRows, nCols);
            k = j + nColsHalf * i;
            if (r >= d0 && r <= d1)
            {
                out[k][0] = 0;
#if PRESERVE_PHASE
                out[k][1] = in[k][1];
#else
                out[k][1] = 0;
#endif
            }
            else
            {
                out[k][0] = in[k][0];
                out[k][1] = in[k][1];
            }
        }
    }
}

/*-----------------------------------------------------------------------**/
void utility::processImageFFT(Image &src, Image &tgt, IPParam param)
{
    assert(!src.empty());
    assert(!tgt.empty());

    // Set image channel where processing is to be performed 
    // depending on the colorspace
    int channel = src.getColorspace() == CS_GREY ? 0 : 2;
#ifdef DEBUG
    printf("processImageFFT() colorspace = %d, color channel = %d\n", src.getColorspace(), channel);
#endif

    // Set dimensions of region to process
    ROI roi = param.roi;
    int nRows = roi.sy;
    int nCols = roi.sx;

    // Store image data into FFT array
    fftw_complex *in = image2fft(src, roi, channel);

    // ================ Run filters here =================
    bool outputDFTAmplitudeOnly = false;
    string outputOpts = param.filter.substr(4,3);
    string filter;
    if (outputOpts == "amp")
    {
        outputDFTAmplitudeOnly = true;
        try
        {
            filter.assign(param.filter.substr(8));
        }
        catch (out_of_range)
        {
            filter.assign("");
        }
    }
    else
    {
        filter.assign(param.filter.substr(4));
    }

    // Allocate memory for output FFT array
    fftw_complex *out = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * nRows * nCols);

    if ((outputOpts == "amp" && filter == "") || filter == "check")
    {
        // Copy input to output FFT
        memcpy(out, in, sizeof(fftw_complex) * nRows * nCols);
    }
    else if (filter == "lowpass")
    {
        fftLowPass(in, out, nRows, nCols, param.filter_params[0]);
    }
    else if (filter == "highpass")
    {
        fftHighPass(in, out, nRows, nCols, param.filter_params[0]);
    }
    else if (filter == "bandpass")
    {
        fftBandPass(in, out, nRows, nCols, param.filter_params[0], 
                param.filter_params[1]);
    }
    else if (filter == "bandstop")
    {
        fftBandStop(in, out, nRows, nCols, param.filter_params[0], 
                param.filter_params[1]);
    }

    // =================================================

    if (outputDFTAmplitudeOnly)
    {
        // Save as amplitude image
#if USE_COMPLEX_AMPLITUDE_DISPLAY
        displayFFTAmplitudeImage(out, nRows * nCols, tgt, roi, channel);
#else
        displayFFTAmplitudeImage2(out, nRows * nCols, tgt, roi, channel);
#endif
        if (channel == 2) // If HSI image, clear H,S channels
        {
            clearImageROIChannel(tgt, roi, 0);
            clearImageROIChannel(tgt, roi, 1);
        }
    }
    else
    {
        // Normalize FFT output by multiplying by a scaling factor
        double scale =  1.0 / (double) (nRows * nCols);
        scaleFFT(out, out, nRows * nCols, scale);

        // Write IFFT data to output image
        fft2image(out, nRows * nCols, tgt, roi, channel);
    }

    // Free allocated memory for FFT
    fftw_free(in);
    fftw_free(out);
}

/*-----------------------------------------------------------------------**/
void utility::addGrey(Image &src, Image &tgt, ROI roi, int value)
{
    assert(!src.empty());
    assert(!tgt.empty()); // target image should have been 'resized' to match source

    //Note: assumes greyscale image.
    //Modifies channel 0 only
    
    for (int i = roi.y; i < roi.y + roi.sy; i++)
    {
        for (int j = roi.x; j < roi.x + roi.sx; j++)
        {
            int pixel = src.getPixel(i, j, 0) + value;
            pixel = pixel > G_MAX ? G_MAX : pixel;
            tgt.setPixel(i, j, 0, pixel);
        }
    }
}

/*-----------------------------------------------------------------------**/
int computeWindowMean(Image &img, ROI roi, int hs, int vs, int i, int j, int channel)
{
    int p;
    int q;
    int n;

    // Assume odd window size (center-reference)
    assert(hs > 0);
    assert(vs > 0);

    // Compute window mean
    int windowMean = 0;
    int value = 0;
    for (int m = -hs; m <= hs; m++)
    {
        for (int n = -vs; n <= vs; n++)
        {
            p = i + m;
            q = j + n;
            if (p < 0 || p >= roi.y + roi.sy || q < 0 || q >= roi.x + roi.sx)
            {
                windowMean += value;
                continue;
            }
            value = img.getPixel(p, q, channel);
            windowMean += value;
        }
    }
    n = (hs * 2 + 1) * (vs * 2 + 1);
    windowMean = (windowMean + n) / n;

    return windowMean;
}

void smoothing(Image &src, Image &tgt, ROI roi, int windowSize)
{
    int channel = src.getColorspace() == CS_GREY ? 0 : 2;

    int windowMean;
    for (int i = roi.y; i < roi.y + roi.sy; i++)
    {
        for (int j = roi.x; j < roi.x + roi.sx; j++)
        {
            // Compute the window mean
            windowMean = computeWindowMean(src, roi, windowSize, windowSize, i, j, channel);
            tgt.setPixel(i, j, channel, windowMean);
        }
    }
}

/*-----------------------------------------------------------------------**/
static const int sobel_mask[2][9] = {{1, 2, 1, 0, 0, 0, -1, -2, -1}, // vertical
                                   {-1,0, 1,-2, 0, 2, -1,  0,  1}}; // horizontal

int sobel(Image &img, int i, int j, int channel, int direction)
{
    int pixel;
    int x,y;
    int sum = 0;
    int m = 0;

    for (int k = -1; k <= 1; k++)
    {
        for (int l = -1; l <= 1; l++)
        {
            y = i + k;
            x = j + l;

            if (x < 0 || x >= img.columns() || y < 0 || y >= img.rows())
            {
                continue;
            }

            pixel = img.getPixel(y, x, channel);
            sum += sobel_mask[direction][m++] * pixel;
        }
    }

    return sum;
}

void edgedetect(Image &src, Image &tgt, ROI roi, int threshold)
{
    int channel = src.getColorspace() == CS_GREY ? 0 : 2;

    int gx, gy;
    double magnitude;
    for (int i = roi.y; i < roi.y + roi.sy; i++)
    {
        for (int j = roi.x; j < roi.x + roi.sx; j++)
        {
            // Get gradient magnitude
            gy = sobel(src, i, j, channel, 0);
            gx = sobel(src, i, j, channel, 1);
            magnitude = sqrt(gy * gy + gx * gx);

            // Scale for display
            magnitude = magnitude > src.maxval(channel) ? src.maxval(channel) : magnitude;

            // Apply partial thresholding
            if (magnitude < threshold)
            {
                magnitude = 0;
            }

            // Write output image
            tgt.setPixel(i, j, channel, (int) magnitude);
        }
    }
}

/*-----------------------------------------------------------------------**/
void utility::processImage(Image &src, Image &tgt, IPParam param)
{
    // FFT-based filters
    if (param.filter.substr(0, 3) == "fft")
    {
        utility::processImageFFT(src, tgt, param);
    }
    else if (param.filter == "addgrey")
    {
        utility::addGrey(src, tgt, param.roi, param.filter_params[0]);
    }
    else if (param.filter == "smoothing")
    {
        smoothing(src, tgt, param.roi, param.filter_params[0]);    
    }
    else if (param.filter == "edgedetect")
    {
        edgedetect(src, tgt, param.roi, param.filter_params[0]);
    }
    else
    {
        stringstream ss;
        ss << "Unknown filter -> " << param.filter << endl;
        string s(ss.str());
        throw IPException(s.c_str());
    }
}

/*-----------------------------------------------------------------------**/
void utility::processImage(string srcPath, string tgtPath, vector<IPParam> params)
{
    // Load source image
    Image *src;
    string ext = getext(srcPath);
    if (ext == "ppm")
    {
        // TODO: For color, defaults to representing image as HSI
        // Need option to specify either RGB or HSI
        src = load(srcPath.c_str(), CS_HSI);
    }
    else
    {
        src = load(srcPath.c_str(), CS_GREY);
    }

    // Copy source image to target image
    Image tgt(*src);

    // Process each parameter line
    vector<IPParam>::iterator iparam;
    for (iparam = params.begin(); iparam != params.end(); iparam++)
    {
        utility::processImage(*src, tgt, *iparam);
    }

    // Save target image
    stringstream ss;
    ss << tgtPath << '.' << ext;
    string tgtFilename(ss.str());
    save(tgt, tgtFilename.c_str());
}

void utility::processVideo(string srcPath, string tgtPath, vector<IPParam> ps,
        COLORSPACE cs, VID_MODE mode)
{
    // Load video
    Video vid(srcPath, cs);

#ifdef DEBUG
    printf("utility::processVideo(...): colorspace = %d\n",
            (vid.getFrame(0))->getColorspace());
#endif

    // Iterate through each parameter block
    vector<IPParam>::iterator iter;
    for (iter = ps.begin(); iter != ps.end(); iter++)
    {
        // Iterate through each frame
        for (int i = 0; i < vid.getFrameCount(); i++)
        {
            // Get frame
            Frame *frame = vid.getFrame(i);

            // Copy frame to target image
            Image tgt((Image) *frame);

            // Process image
            utility::processImage((Image &) *frame, tgt, *iter);

            // Copy target image to frame
            vid.setFrameData(i, tgt);
        }
    }

    // Save video
    string ext = getext(srcPath);
    stringstream ss;
    ss << tgtPath << '.' << ext;
    vid.saveVideo(ss.str());

    // Close video
    vid.close();
}


