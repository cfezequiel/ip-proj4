Parameter File Format
-------------------------

The parameter file (.p) contains the regions where processing should be
performed on the image or video and the filter parameters. 
The format is as follows:

    N 
    x y sx sy soiStart soiEnd filter param0 param1 ... paramM
    ...

    noindent where:
    N - number of ROIs
    x - column index of top-left-most pixel of ROI
    y - row index of top-left-most pixel of ROI
    sx - size of ROI along the columns
    sy - size of ROI along the rows
    soiStart - beginning frame of sequence-of-interest (SOI)
    soiEnd - ending frame of SOI
    filter - name of the filter (i.e. 'greythesh', 'colorthresh', etc.)
    param0 - first parameter of filter
    param1 - second parameter of filter
    ...
    paramM - last parameter of filter


Example:

2
30 30  670 210 1   130 colorthresh 10 112 80 46
30 200 670 160 130 200 colorthresh 10 112 80 46

The example above shows the parameter file containing 2 ROIs. 
The first ROI (second line) has top-left x,y-position of (30, 30) with horizontal size of 670 and vertical size of 210. 
If a video stream is given, the filter should be applied in the ROI at frames 1 to 130. 
The filter used is color thresholding (colorthresh). 
The threshold is 10 and the reference RGB-color is (112, 80, 46). 
The second ROI appears below the first, and the filter should be applied to the ROI at frame count 130 to 200. 
The same color thresholding filter is to be used with the same parameters as the first ROI.


